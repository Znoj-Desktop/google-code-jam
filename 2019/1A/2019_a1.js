/**
 * Round 1A 2019 - Google Code Jam 2019
 * Pylons (8pts, 23pts)
 * POSSIBLE / IMPOSSIBLE and number of results exactly the same as script provided by Google, but result is WA
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let testCases;

const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream("./2019/1A/input"),
  output: process.stdout,
  terminal: false,
});

rl.on("line", processLine);

function processLine(line) {
  if (!testCases) {
    testCases = +line;
    return;
  } else if (caseIndex > testCases) {
    rl.close();
    return;
  } else {
    const [rr, cc] = line.split(" ");
    const Rmax = +rr;
    const Cmax = +cc;
    if (Rmax < 4 && Cmax < 4) {
      process.stdout.write(`Case #${caseIndex++}: IMPOSSIBLE\n`);
      return;
    } else {
      let r = 0;
      let c = 0;
      if (Cmax === 2) {
        if (Rmax < 5) {
          process.stdout.write(`Case #${caseIndex++}: IMPOSSIBLE\n`);
          return;
        }
        process.stdout.write(`Case #${caseIndex++}: POSSIBLE\n`);
        for (let r = 0; r < Rmax; r += 1) {
          for (let c = 0; c < Cmax; c += 2) {
            process.stdout.write(`${r + 1} ${c + 1}\n`);
            if (c + 1 < Cmax) {
              process.stdout.write(
                `${((r + 3) % Rmax) + 1} ${((c + 1) % Cmax) + 1}\n`
              );
            }
          }
        }
      } else if (Rmax === 2) {
        if (Cmax < 5) {
          process.stdout.write(`Case #${caseIndex++}: IMPOSSIBLE\n`);
          return;
        }
        process.stdout.write(`Case #${caseIndex++}: POSSIBLE\n`);
        for (let c = 0; c < Cmax; c += 1) {
          for (let r = 0; r < Rmax; r += 2) {
            process.stdout.write(`${r + 1} ${c + 1}\n`);
            if (r + 1 < Rmax) {
              process.stdout.write(
                `${((r + 1) % Rmax) + 1} ${((c + 3) % Cmax) + 1}\n`
              );
            }
          }
        }
      } else if (Cmax === 3) {
        process.stdout.write(`Case #${caseIndex++}: POSSIBLE\n`);
        for (let r = 0; r < Rmax; r += 1) {
          for (let c = 0; c < Cmax; c += 2) {
            process.stdout.write(`${r + 1} ${c + 1}\n`);
            if (c + 1 < Cmax) {
              process.stdout.write(
                `${((r + 2) % Rmax) + 1} ${((c + 1) % Cmax) + 1}\n`
              );
            }
          }
        }
        // process.stdout.write(`${r + Math.floor(Rmax / 2)} ${c}\n`);
      } else {
        process.stdout.write(`Case #${caseIndex++}: POSSIBLE\n`);
        for (let c = 0; c < Cmax; c += 1) {
          for (let r = 0; r < Rmax; r += 2) {
            process.stdout.write(`${r + 1} ${c + 1}\n`);
            if (r + 1 < Rmax) {
              process.stdout.write(
                `${((r + 1) % Rmax) + 1} ${((c + 2) % Cmax) + 1}\n`
              );
            }
          }
        }
      }
    }
  }
}
