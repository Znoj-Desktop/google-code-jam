/**
 * Round 1A 2019 - Google Code Jam 2019
 * Golf Gophers (11pts, 21pts)
 * earn: 11pts
 *
 */

const fs = require("fs");
const readline = require("readline");
const fills = [12, 13, 14, 15, 16, 17, 18];
const results = [];
let testCases;
let nights;
let maxGophers;
let index = 0;
let maxIndex = fills.length;
const MILLS = 18;

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2019/1A/2019_a2_in"),
  input: fs.createReadStream("./2019_a2_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

function processLine(line) {
  if (!testCases) {
    const [t, n, m] = line.split(" ");
    testCases = +t;
    nights = +n;
    maxGophers = +m;
    console.log(`${arrayToString(guessArray(fills[index++]))}`);
    return;
  }
  if (+line === 1) {
    // correct
    // console.log(`correct`); // TODO remove
    clean();
    if (testCases <= 0) {
      exit();
      return;
    } else {
      console.log(`${arrayToString(guessArray(fills[index++]))}`);
    }
  } else if (+line === -1) {
    // false and exit
    exit();
    return;
  } else {
    const response = line.split(" ");
    results[index - 1] = +count(response);
    if (index < maxIndex) {
      console.log(`${arrayToString(guessArray(fills[index++]))}`);
    } else {
      for (let g = 0; g < Math.floor(100 / 18); g++) {
        for (let f = 0; f < Math.floor(100 / 17); f++) {
          for (let e = 0; e < Math.floor(100 / 16); e++) {
            for (let d = 0; d < Math.floor(100 / 15); d++) {
              for (let c = 0; c < Math.floor(100 / 14); c++) {
                for (let b = 0; b < Math.floor(100 / 13); b++) {
                  for (let a = 0; a < Math.floor(100 / 12); a++) {
                    if (g * 18 + results[6] === f * 17 + results[5]) {
                      if (g * 18 + results[6] === e * 16 + results[4]) {
                        if (f * 17 + results[5] === d * 15 + results[3]) {
                          if (f * 17 + results[5] === c * 14 + results[2]) {
                            if (f * 17 + results[5] === b * 13 + results[1]) {
                              if (f * 17 + results[5] === a * 12 + results[0]) {
                                console.log(`${g * 18 + results[6]}`);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

const arrayToString = (array) => {
  let output = "";
  for (let i = 0; i < array.length; i++) {
    output += array[i];
    if (i < array.length - 1) {
      output += " ";
    }
  }
  return output;
};

const guessArray = (numToFillIn) => {
  let guess = [];
  for (let i = 0; i < MILLS; i++) {
    guess.push(numToFillIn);
  }
  return guess;
};

const clean = () => {
  testCases--;
  index = 0;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};

const count = (arrayOfNumbers) => {
  let output = 0;
  for (let i = 0; i < arrayOfNumbers.length; i++) {
    output += +arrayOfNumbers[i];
  }
  return output;
};
