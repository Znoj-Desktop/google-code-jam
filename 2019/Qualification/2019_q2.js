/**
 * Qualification Round 2019 - Google Code Jam 2019
 * You Can Go Your Own Way (5pts, 9pts, 10pts)
 * earn 24 pts
 */

const readline = require("readline");
let caseIndex = 1;
let lines;
let dimension;

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false,
});

rl.on("line", processLine);

function processLine(line) {
  if (!lines) {
    lines = +line;
    return;
  }
  if (!dimension) {
    dimension = +line;
    return;
  } else {
    let result = "";
    for (let i = 0; i < String(line).length; i++) {
      result += line[i] === "E" ? "S" : "E";
    }
    process.stdout.write(`Case #${caseIndex++}: ${result}\n`);

    dimension = undefined;
  }
}
