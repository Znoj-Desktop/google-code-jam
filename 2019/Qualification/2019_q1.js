/**
 * Qualification Round 2019 - Google Code Jam 2019
 * Foregone Solution (6pts, 10pts, 1pts)
 * passing Test set 1 (Visible)
 */
/*
const readline = require('readline');
let lines;
let caseIndex = 1;


const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

rl.on('line', processLine)

function processLine (line) {
    if(!lines){
        lines = +line;
        return;
    }
    else{
        let a;
        let b;
        for(let i = 1; i < +line; i++){
            a = i;
            b = +line - i;
            if(String(a).indexOf('4') >= 0){
                continue;
            }
            if(String(b).indexOf('4') >= 0){
                continue;
            }
            process.stdout.write(`Case #${caseIndex++}: ${a} ${b}\n`);

            if(caseIndex > lines){
                rl.close();
                return;
            }
            break;
        }
    }

}
*/

/**
 * Qualification Round 2019 - Google Code Jam 2019
 * Foregone Solution (6pts, 10pts, 1pts)
 * passing Test set 2 (Visible)
 */

/*
const readline = require('readline');
let lines;
let caseIndex = 1;


const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

rl.on('line', processLine)

function processLine (line) {
    if(!lines){
        lines = +line;
        return;
    }
    else{
        let a, aIndex, aLength, b, bIndex, bLength;
        for(let i = 1; i < +line; i++){
            a = i;
            b = +line - i;

            aIndex = String(a).indexOf('4');
            aLength = String(a).length;
            bIndex = String(b).indexOf('4');
            bLength = String(b).length;
            
            if(aIndex >= 0){
                console.log('a: ' + a + ', i: ' + i + ', aIndex: ' + aIndex + ', aLength: ' + aLength);
                i += 10**(+aLength - +aIndex - 1) -1;
                // console.log('a: ' + a + ', i: ' + i);
                // console.log('---');
                continue;
            }
            if(bIndex >= 0){
                console.log('b: ' + b + ', b: ' + i + ', bIndex: ' + bIndex + ', bLength: ' + bLength);
                i += 10**(+bLength - +bIndex - 1) -1;
                // console.log('b: ' + b + ', b: ' + i);
                // console.log('---');
                continue;
            }
            process.stdout.write(`Case #${caseIndex++}: ${a} ${b}\n`);

            if(caseIndex > lines){
                rl.close();
                return;
            }
            break;
        }
    }

}
*/

/**
 * Qualification Round 2019 - Google Code Jam 2019
 * Foregone Solution (6pts, 10pts, 1pts)
 * passing Test set 3 (Visible)
 * WA - something is missing, but don't know what. So earn 16 pts with this
 */
const readline = require("readline");
const fs = require("fs");
let lines;
let caseIndex = 1;

const rl = readline.createInterface({
  input: process.stdin,
  //    input: fs.createReadStream('./2019_q1_in'),
  output: process.stdout,
  terminal: false,
});

rl.on("line", processLine);

function processLine(line) {
  if (!lines) {
    lines = +line;
    return;
  } else {
    let parts = [];
    if (line.length > 9) {
      for (let i = 0; i < Math.floor(line.length / 10); i++) {
        parts[i] = line.slice(i * 10, (i + 1) * 10);
      }
      if (line.length % 10 !== 0) {
        parts[Math.floor(line.length / 10)] = line.slice(
          Math.floor(line.length / 10) * 10
        );
      }
      // console.log('parts', parts)
    } else {
      parts[0] = line;
    }
    let a = [],
      aIndex,
      aLength,
      b = [],
      bIndex,
      bLength;
    for (let p = 0; p < parts.length; p++) {
      for (let i = 0; i < +parts[p]; i++) {
        a[p] = i;
        b[p] = +parts[p] - i;

        aIndex = String(a[p]).indexOf("4");
        aLength = String(a[p]).length;
        bIndex = String(b[p]).indexOf("4");
        bLength = String(b[p]).length;

        if (aIndex >= 0) {
          // console.log('a: ' + a[p] + ', i: ' + i + ', aIndex: ' + aIndex + ', aLength: ' + aLength);
          i += 10 ** (+aLength - +aIndex - 1) - 1;
          // console.log('a: ' + a + ', i: ' + i);
          // console.log('---');
          continue;
        }
        if (bIndex >= 0) {
          // console.log('b: ' + b[p] + ', b: ' + i + ', bIndex: ' + bIndex + ', bLength: ' + bLength);
          i += 10 ** (+bLength - +bIndex - 1) - 1;
          // console.log('b: ' + b + ', b: ' + i);
          // console.log('---');
          continue;
        }
        // console.log('a: ' + a[0]);
        // console.log('b: ' + b[0]);
        break;
      }
      if (a[p] === undefined) {
        a[p] = 0;
      }
      if (b[p] === undefined) {
        b[p] = 0;
      }
    }
    let num1 = "",
      num2 = "";
    if (parts.length > 1) {
      let aLength, bLength;
      for (let p = 0; p < parts.length - 1; p++) {
        aLength = String(a[p]).length;
        for (let i = 9; i >= aLength; i--) {
          num1 = num1 + "0";
        }
        num1 = num1 + String(a[p]);
        bLength = String(b[p]).length;
        for (let i = 9; i >= bLength; i--) {
          num2 = num2 + "0";
        }
        num2 = num2 + String(b[p]);
        // console.log('aLength: ' +aLength);
        // console.log('a[p]: ' +a[p]);
        // console.log('bLength: ' +bLength);
        // console.log('b[p]: ' +b[p]);
      }

      aLength = String(a[parts.length - 1]).length;
      bLength = String(b[parts.length - 1]).length;
      // console.log('num1: ' +aLength);
      // console.log('bLength: ' +bLength);

      let maxLength = aLength;
      if (bLength > maxLength) {
        maxLength = bLength;
        for (let i = maxLength; i > aLength; i--) {
          num1 = num1 + "0";
          // console.log('0a: ' + num1);
        }
      } else {
        for (let i = maxLength; i > bLength; i--) {
          num2 = num2 + "0";
          // console.log('0b: ' + num2);
        }
      }
      num1 = num1 + String(a[parts.length - 1]);
      num2 = num2 + String(b[parts.length - 1]);
    } else {
      num1 = String(a[0]);
      num2 = String(b[0]);
    }

    process.stdout.write(`Case #${caseIndex++}: ${num1} ${num2}\n`);

    if (caseIndex > lines) {
      rl.close();
      return;
    }
  }
}
