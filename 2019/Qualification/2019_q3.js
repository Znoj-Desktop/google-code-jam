/**
 * Qualification Round 2019 - Google Code Jam 2019
 * Cryptopangrams (10pts, 15pts)
 * WA, but visible tests pass
 * earn 0 pts
 */
const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let lines;
let N, L;

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false,
});

rl.on("line", processLine);

function processLine(line) {
  if (!lines) {
    lines = +line;
    return;
  }
  if (!N) {
    let input = String(line).split(" ");
    N = input[0];
    L = input[1];
    return;
  } else {
    let allPrimaryNumbers = [];
    outer: for (let i = 2; i < N; i++) {
      for (let p = 0; p < allPrimaryNumbers.length; p++) {
        if (i % allPrimaryNumbers[p] === 0) {
          continue outer;
        }
      }
      allPrimaryNumbers.push(i);
    }
    allPrimaryNumbers.push(1);

    let primaryNumbers = [];
    let input = String(line).split(" ");
    outer: for (let i = 0; i < input.length; i++) {
      if (primaryNumbers.length === 0) {
        for (let p = 0; p < allPrimaryNumbers.length; p++) {
          if (+input[i] % allPrimaryNumbers[p] === 0) {
            let num1 = +allPrimaryNumbers[p];
            let num2 = +input[i] / +allPrimaryNumbers[p];
            if (+input[i + 1] % num2 !== 0) {
              let num3 = num1;
              num1 = num2;
              num2 = num3;
            }

            primaryNumbers.push(num1);
            primaryNumbers.push(num2);
            continue outer;
          }
        }
      }

      primaryNumbers.push(
        +input[i] / primaryNumbers[primaryNumbers.length - 1]
      );
    }

    let primary = [...primaryNumbers];
    let primarySorted = [...primary]
      .filter(function (item, pos) {
        return primary.indexOf(item) == pos;
      })
      .sort((a, b) => a - b);

    // console.log(primary);
    // console.log(primarySorted);

    let output = "";
    for (let i = 0; i < primary.length; i++) {
      // console.log(primarySorted.indexOf(primary[i]));
      output += String.fromCharCode(primarySorted.indexOf(primary[i]) + 65);
    }

    process.stdout.write(`Case #${caseIndex++}: ${output}\n`);
    N = undefined;
  }
}
