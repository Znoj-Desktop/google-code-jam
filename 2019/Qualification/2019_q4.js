/**
 * Qualification Round 2019 - Google Code Jam 2019
 * Dat Bae (14pts, 20pts)
 * earn: ?pts
 * Pass known input, RuntimeError due to spread operator probably
 */

const fs = require("fs");
const readline = require("readline");
let TtestCases;
let Nworkers, BrokenWorkers, FlinesToSend;

const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream("./input"),
  output: process.stdout,
  terminal: false,
});

rl.on("line", processLine);

let linesSent = 0;
let r = {};
let brokenWorkers = [];

function processLine(line) {
  if (!TtestCases) {
    TtestCases = +line;
    return;
  } else if (!Nworkers) {
    const numbers = line.match(/(\d+)\D+(\d+)\D+(\d+)/);
    Nworkers = +numbers[1];
    BrokenWorkers = +numbers[2];
    FlinesToSend = +numbers[3];

    // 1
    const output = leftHalfZeros(Nworkers);
    r[linesSent] = {
      output: [output],
      send: output,
    };
    process.stdout.write(`${output[0] + output[1]}\n`);
    linesSent++;
  } else {
    let cur = linesSent - 1;
    r[cur] = {
      ...r[cur],
      get: line,
      broken: [],
      correct: [],
    };

    // should be put to next condition to simplify alg
    if (linesSent === 1) {
      let halfLength = Math.floor(Nworkers / 2);
      let halfLeft = halfLength + (Nworkers % 2);

      const count0 = countChars(line, "0");
      const count1 = countChars(line, "1");

      r[cur].broken[0] = [
        countChars(r[cur].output[0][0], "0") - count0,
        countChars(r[cur].output[0][1], "1") - count1,
      ];
      r[cur].correct[0] = [count0, count1];

      const outputL = leftHalfZeros(halfLeft);
      const outputR = leftHalfZeros(halfLength);
      let output = outputL[0] + outputL[1] + outputR[0] + outputR[1];
      r[linesSent] = {
        ...r[linesSent],
        output: [outputL, outputR],
        send: output,
      };

      let c = 0;

      if (r[cur].correct[c][0] === 0 && r[cur].broken[c][0] !== 0) {
        let prev = 0;
        let start = prev;
        let end = start + r[cur].output[c][0].length;

        addBrokenWorkers(start, end);
        if (brokenWorkers.length === BrokenWorkers) {
          allWorkersRevealed();
          return;
        }
      }
      if (r[cur].correct[c][1] === 0 && r[cur].broken[c][1] !== 0) {
        let prev = 0;
        let start = prev + r[cur].output[c][0].length;
        let end = start + r[cur].output[c][1].length;

        addBrokenWorkers(start, end);
        if (brokenWorkers.length === BrokenWorkers) {
          allWorkersRevealed();
          return;
        }
      }

      process.stdout.write(`${output}\n`);
      linesSent++;
    } else if (linesSent > 1) {
      // analyze

      let prev = 0;
      let allPrevLength = 0;
      for (let c = 0; c < 2 ** cur; c++) {
        let outArrayIndex = Math.floor(c / 2);

        const count0ForThis = countChars(
          line.substr(prev, r[cur - 1].correct[outArrayIndex][c % 2]),
          "0"
        );
        const count1ForThis = countChars(
          line.substr(prev, r[cur - 1].correct[outArrayIndex][c % 2]),
          "1"
        );

        const prevAllCorrect = r[cur - 1].broken[outArrayIndex][c % 2] === 0;
        const prevAllBroken = r[cur - 1].correct[outArrayIndex][c % 2] === 0;

        r[cur].broken[c] = [
          prevAllCorrect
            ? 0
            : prevAllBroken
            ? countChars(r[cur].output[c][0], "0")
            : countChars(r[cur].output[c][0], "0") - count0ForThis,
          prevAllCorrect
            ? 0
            : prevAllBroken
            ? countChars(r[cur].output[c][1], "1")
            : countChars(r[cur].output[c][1], "1") - count1ForThis,
        ];
        r[cur].correct[c] = [
          prevAllCorrect
            ? countChars(r[cur].output[c][0], "0")
            : prevAllBroken
            ? 0
            : count0ForThis,
          prevAllBroken
            ? countChars(r[cur].output[c][1], "1")
            : prevAllBroken
            ? 0
            : count1ForThis,
        ];

        if (r[cur].correct[c][0] === 0 && r[cur].broken[c][0] !== 0) {
          let prev = allPrevLength;
          let start = prev;
          let end = start + r[cur].output[c][0].length;

          addBrokenWorkers(start, end);
          if (brokenWorkers.length === BrokenWorkers) {
            allWorkersRevealed();
            return;
          }
        }
        if (r[cur].correct[c][1] === 0 && r[cur].broken[c][1] !== 0) {
          let prev = allPrevLength;
          let start = prev + r[cur].output[c][0].length;
          let end = start + r[cur].output[c][1].length;

          addBrokenWorkers(start, end);
          if (brokenWorkers.length === BrokenWorkers) {
            allWorkersRevealed();
            return;
          }
        }

        if (c % 2 === 0) {
          prev = prev + r[cur - 1].correct[outArrayIndex][0];
        } else {
          prev = prev + r[cur - 1].correct[outArrayIndex][1];
        }

        allPrevLength =
          allPrevLength +
          r[cur].output[c][0].length +
          r[cur].output[c][1].length;
      }

      let output = "";
      for (let i = 0; i < r[cur].output.length; i++) {
        const outputL = leftHalfZeros(r[cur].output[i][0].length);
        const outputR = leftHalfZeros(r[cur].output[i][1].length);
        output += outputL[0] + outputL[1] + outputR[0] + outputR[1];

        r[linesSent] = {
          ...r[linesSent],
          output:
            r[linesSent] && r[linesSent].output
              ? [...r[linesSent].output, outputL, outputR]
              : [outputL, outputR],
          send: output,
        };
      }
      process.stdout.write(`${output}\n`);
      linesSent++;
    }
  }
}

function leftHalfZeros(N) {
  let output = ["", ""];
  if (N.length === 1) {
    return output;
  }
  for (let i = 0; i < N; i++) {
    if (i < N / 2) {
      output[0] += "0";
    } else {
      output[1] += "1";
    }
  }
  return output;
}

function countChars(line, char) {
  let chars = 0;
  for (let i = 0; i < line.length; i++) {
    line[i] === char && chars++;
  }
  return chars;
}

function addBrokenWorkers(start, end) {
  for (let i = start; i < end; i++) {
    if (brokenWorkers.indexOf(i) >= 0) {
      continue;
    }
    brokenWorkers.push(i);
  }
}

function allWorkersRevealed() {
  let bOut = "";
  brokenWorkers.sort().forEach((bw) => {
    bOut += bw + " ";
  });
  process.stdout.write(`${bOut.substr(0, bOut.length - 1)}\n`);
  clean();
  if (TtestCases <= 0) {
    process.stdout.write(`exit\n`);
  }
}

function clean() {
  TtestCases--;
  Nworkers = 0;
  linesSent = 0;
  r = {};
  brokenWorkers = [];
}
