// with extension https://marketplace.visualstudio.com/items?itemName=miramac.vscode-exec-node&ssr=false#overview
// Run it with F8 (focus in file), stop it with F9
// put test data into file 'input'
// comment input and put process.stdin for production

/*
const fs = require('fs');
const readline = require('readline');
let caseIndex = 1;

const rl = readline.createInterface({
  // input: process.stdin,
  input: fs.createReadStream('./input'),
  output: process.stdout,
  terminal: false
});

rl.on('line', processLine)

function processLine(line) {
  process.stdout.write(`Case #${caseIndex++}: ${line}\n`);

}
*/

// OR
/**
 * Qualification Round 2019 - Google Code Jam 2019
 * Dat Bae (14pts, 20pts)
 * earn: pts
 *
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let lines;

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/2020_q1_in"),
  input: fs.createReadStream("./2020_q1_in"),
  output: process.stdout
});

rl.on("line", processLine).on("close", function() {
  process.exit(0);
});

function processLine(line) {
  if (!lines) {
    lines = +line;
    return;
  } else {
    console.log(`Case #${caseIndex++}: ${line}`);
  }
}

const clean = () => {
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
