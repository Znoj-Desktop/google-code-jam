/**
 * A1 Round 2020 - Google Code Jam 2020
 * Pattern Matching (5pts, 5pts, 18pts)
 * result: 10pts
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let testCases;
let lines = 0;
let masterString = "";
let masterStart = "";
let masterEnd = "";
let error = false;

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/2020_q1_in"),
  input: fs.createReadStream("./2020_a1_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

function processLine(line) {
  if (!testCases) {
    testCases = +line;
    return;
  } else if (lines === 0) {
    lines = +line;
    error = false;
    masterString = "";
    return;
  } else {
    lines--;
    const newWord = line;
    if (error) {
      if (lines === 0) {
        console.log(`Case #${caseIndex++}: *`);
      }
      return;
    }
    if (masterString === "") {
      masterString = line.split("*");
      masterStart = masterString[0];
      masterEnd = masterString[1].split("").reverse().join("");
      // if (lines === 0) {
      //   console.log(`Case #${caseIndex++}: ${masterString}`);
      // }
      // return;
    } else {
      const splitWord = newWord.split("*");
      const newStart = splitWord[0];
      const newEnd = splitWord[1].split("").reverse().join("");
      for (let i = 0; i < newEnd.length; i++) {
        if (i > masterEnd.length - 1) {
          masterString = newWord.split("*");
          masterEnd = masterString[1].split("").reverse().join("");
          break;
        }
        if (newEnd[i] !== masterEnd[i]) {
          error = true;
        }
      }
      for (let i = 0; i < newStart.length; i++) {
        if (i > masterStart.length - 1) {
          masterString = newWord.split("*");
          masterStart = masterString[0];
          break;
        }
        if (newStart[i] !== masterStart[i]) {
          error = true;
        }
      }
    }

    if (lines === 0) {
      if (error) {
        console.log(`Case #${caseIndex++}: *`);
      } else {
        console.log(
          `Case #${caseIndex++}: ${masterStart}${masterEnd
            .split("")
            .reverse()
            .join("")}`
        );
      }
    }
  }
}
const clean = () => {
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
