/**
 * A1 Round 2020 - Google Code Jam 2020
 * Pascal Walk (3pts, 11pts, 21pts)
 * result: 14
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let testCases;
let lines = 0;
const trace = [];

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/A1/2020_a2_in"),
  input: fs.createReadStream("./2020_a2_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

function processLine(line) {
  if (!testCases) {
    testCases = +line;
    return;
  } else {
    lines--;
    const N = +line;
    console.log(`Case #${caseIndex++}:`);
    if (N < 250) {
      for (let i = 1; i < N + 1; i++) {
        console.log(`${i} 1`);
      }
    } else {
      // TODO - there could be more than 500 positions for bigger numbers
      for (let k = 3; k < 35; k++) {
        if (N < Math.pow(2, k) + Math.pow(2, k - 1)) {
          for (let i = 1; i < k + 1; i++) {
            console.log(`${i} ${i}`);
          }
          for (let i = k - 1; i > 1; i--) {
            console.log(`${k} ${i}`);
          }
          for (let i = k; i < N - Math.pow(2, k - 1) + 2; i++) {
            console.log(`${i} 1`);
          }
          break;
        }
      }
    }
  }
}
const clean = () => {
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
