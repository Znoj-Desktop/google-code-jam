/**
 * Qualification Round 2020 - Google Code Jam 2020
 * Nesting Depth (5pts, 11pts)
 * result: DONE
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let lines;

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/2020_q2_in"),
  input: fs.createReadStream("./2020_q2_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

function processLine(line) {
  if (!lines) {
    lines = +line;
    return;
  } else {
    let result = "";
    let openedParenthesis = 0;
    for (let i = 0; i < line.length; i++) {
      let num = +line[i];
      // if (openedParenthesis === num) {
      //   result += num;
      // }
      if (openedParenthesis < num) {
        while (openedParenthesis < num) {
          result += "(";
          openedParenthesis++;
        }
      }
      if (openedParenthesis > num) {
        while (openedParenthesis > num) {
          result += ")";
          openedParenthesis--;
        }
      }
      result += num;
    }
    while (openedParenthesis > 0) {
      result += ")";
      openedParenthesis--;
    }
    console.log(`Case #${caseIndex++}: ${result}`);
  }
}

const clean = () => {
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
