/**
 * Qualification Round 2020 - Google Code Jam 2020
 * Parenting Partnering Returns (7pts, 12pts)
 * result: DONE
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let testCases;
let activities = 0;
const activitiesSchedule = [];

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/2020_q3_in"),
  input: fs.createReadStream("./2020_q3_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

function processLine(line) {
  if (!testCases) {
    testCases = +line;
    return;
  } else if (activities === 0) {
    activities = +line;
    return;
  } else {
    activitiesSchedule.push(line.split(" "));
    activities--;
    if (activities === 0) {
      const activitiesScheduleSorted = [...activitiesSchedule];
      // sort
      for (let i = 0; i < activitiesScheduleSorted.length; i++) {
        let min = +activitiesScheduleSorted[i][0];
        for (let j = i; j < activitiesScheduleSorted.length; j++) {
          if (+activitiesScheduleSorted[j][0] < min) {
            min = +activitiesScheduleSorted[j][0];
            let pom = activitiesScheduleSorted[i];
            activitiesScheduleSorted[i] = activitiesScheduleSorted[j];
            activitiesScheduleSorted[j] = pom;
          }
        }
        // console.log(`SORTED: ${activitiesScheduleSorted[i]}`);
      }

      const C = [activitiesScheduleSorted[0]];
      const J = [];

      for (let i = 1; i < activitiesScheduleSorted.length; i++) {
        // console.log(`+C #${+C[C.length - 1][1]}`);
        // console.log(`+J #${J.length > 0 && +J[J.length - 1][1]}`);
        // console.log(
        //   `+activitiesScheduleSorted[${i}][0] #${+activitiesScheduleSorted[i][0]}`
        // );
        if (+C[C.length - 1][1] <= +activitiesScheduleSorted[i][0]) {
          C.push(activitiesScheduleSorted[i]);
        } else if (
          J.length === 0 ||
          +J[J.length - 1][1] <= +activitiesScheduleSorted[i][0]
        ) {
          J.push(activitiesScheduleSorted[i]);
        } else {
          clean();
          console.log(`Case #${caseIndex++}: IMPOSSIBLE`);
          return;
        }
      }
      let result = "";
      for (let i = 0; i < activitiesSchedule.length; i++) {
        // console.log("i", i);
        // console.log("activitiesSchedule[i]", activitiesSchedule[i]);
        for (let k = 0; k < C.length; k++) {
          // console.log("C[k]", C[k]);
          if (activitiesSchedule[i] === C[k]) {
            result += "C";
            break;
          }
        }
        for (let k = 0; k < J.length; k++) {
          // console.log("J[k]", J[k]);
          if (activitiesSchedule[i] === J[k]) {
            result += "J";
            break;
          }
        }
      }

      clean();
      console.log(`Case #${caseIndex++}: ${result}`);
    }
  }
}

const clean = () => {
  activitiesSchedule.length = 0;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
