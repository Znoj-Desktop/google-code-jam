/**
 * Qualification Round 2020 - Google Code Jam 2020
 * ESAb ATAd (1pts, 9pts, 16pts)
 * result: 1b - didn't finish it
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let testCases;
let BITS;
let bitsSent = 0;
const bitsArray = [];
const bitsVariations = [];

const rl = readline.createInterface({
  input: process.stdin,
  // input: fs.createReadStream("./2020/2020_q4_in"),
  // input: fs.createReadStream("./2020_q4_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

function processLine(line) {
  if (!testCases) {
    const [t, b] = line.split(" ");
    testCases = +t;
    BITS = +b;

    sendFirst();
    return;
  } else if (line === "N") {
    exit();
    return;
  } else if (line === "Y") {
    testCases--;
    if (testCases === 0) {
      exit();
      return;
    } else {
      sendFirst();
      bitsVariations.length = 0;
    }
  } else {
    bitsArray.push(+line);
    if (bitsVariations.length > 0 && bitsArray.length === Math.floor(BITS)) {
      // console.log(`bitsVariations[0] ${bitsVariations[0]}`);
      // console.log(`bitsVariations[1] ${bitsVariations[1]}`);
      // console.log(`bitsVariations[2] ${bitsVariations[2]}`);
      // console.log(`bitsVariations[3] ${bitsVariations[3]}`);
      // console.log(`bitsArray${bitsArray}`);
      // console.log(`BITS${BITS}`);
      // console.log(`${bitsSent++}`);
      for (let i = 0; i < bitsArray.length; i++) {
        if (bitsVariations[0][i] !== bitsArray[i]) {
          break;
        }
        if (i === bitsArray.length - 1) {
          console.log(`${arrayToPrint(bitsVariations[0])}`);
          return;
        }
      }
      for (let i = 0; i < bitsArray.length; i++) {
        if (bitsVariations[1][i] !== bitsArray[i]) {
          break;
        }
        if (i === bitsArray.length - 1) {
          console.log(`${arrayToPrint(bitsVariations[1])}`);
          return;
        }
      }
      for (let i = 0; i < bitsArray.length; i++) {
        if (bitsVariations[2][i] !== bitsArray[i]) {
          break;
        }
        if (i === bitsArray.length - 1) {
          console.log(`${arrayToPrint(bitsVariations[2])}`);
          return;
        }
      }
      for (let i = 0; i < bitsArray.length; i++) {
        if (bitsVariations[3][i] !== bitsArray[i]) {
          break;
        }
        if (i === bitsArray.length - 1) {
          console.log(`${arrayToPrint(bitsVariations[3])}`);
          return;
        }
      }
    } else if (bitsSent === BITS) {
      // create bitsVariations
      bitsVariations[0] = [...bitsArray];
      bitsVariations[1] = [];
      bitsVariations[2] = [];
      bitsVariations[3] = [];
      for (let i = 0; i < bitsArray.length; i++) {
        const mirrorVar = bitsArray[bitsArray.length - 1 - i];
        bitsVariations[1][i] = +bitsArray[i] === 1 ? 0 : 1;
        bitsVariations[2][i] = bitsArray[bitsArray.length - 1 - i];
        bitsVariations[3][i] = +mirrorVar === 1 ? 0 : 1;
      }

      bitsSent = 1;
      // console.log(`Case #${caseIndex++}: ${bitsArray}`);
      sendFirst();
    } else {
      // console.log(`bitsSent ${bitsSent}`);
      // console.log(`bits ${bits}`);
      console.log(`${++bitsSent}`);
      // bitsSent++;
    }
  }
}

const sendFirst = () => {
  // send first
  console.log(1);
  bitsSent = 1;
  bitsArray.length = 0;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};

const arrayToPrint = (array) => {
  let response = "";
  for (let i = 0; i < array.length; i++) {
    response += "" + array[i];
  }
  return response;
};
