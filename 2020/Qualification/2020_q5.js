/**
 * Qualification Round 2020 - Google Code Jam 2020
 * Indicium (7pts, 25pts)
 * result: WA - all posible cases covered by tests and manually checked for first level of points, bud WA
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let testCases;
let N;
let K;

//#region help function
const possible = (matrix) => {
  console.log(`Case #${caseIndex++}: POSSIBLE`);
  for (let r = 0; r < N; r++) {
    let colRow = "";
    for (let c = 0; c < N; c++) {
      colRow += matrix[r][c];
      if (c !== N - 1) {
        colRow += " ";
      }
    }
    console.log(colRow);
  }
};

const impossible = () => {
  console.log(`Case #${caseIndex++}: IMPOSSIBLE`);
};

const isInRow = (matrix, row, num) => {
  for (let i = 0; i < N; i++) {
    if (matrix[row][i] === num) {
      return true;
    }
  }
  return false;
};

const isInCol = (matrix, col, num) => {
  for (let i = 0; i < N; i++) {
    if (matrix[i][col] === num) {
      return true;
    }
  }
  return false;
};
//#endregion

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/2020_q5_in"),
  input: fs.createReadStream("./2020_q5_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

function processLine(line) {
  if (!testCases) {
    testCases = +line;
    return;
  } else if (caseIndex > testCases) {
    rl.close();
    return;
  } else {
    const [nIn, kIn] = line.split(" ");
    N = +nIn;
    K = +kIn;

    //#region check
    let diagonalNumbers = [];
    let dividedK = K;
    for (let i = N; i > 1; i--) {
      if (dividedK % i === 0) {
        diagonalNumbers.push(i);
        dividedK = dividedK / i;
        i++;
      }
    }
    if (diagonalNumbers.length === 1) {
      const num = +diagonalNumbers[0];
      for (let i = 2; i <= Math.sqrt(num); i++) {
        if (num % i === 0) {
          diagonalNumbers[0] = num / i;
          diagonalNumbers.push(i);
          break;
        }
      }
    }
    if (diagonalNumbers.length === 1) {
      impossible();
      return;
    }
    if (diagonalNumbers.length === N - 1) {
      let temp = diagonalNumbers[0];
      let possible = false;
      for (let i = 1; i < diagonalNumbers.length; i++) {
        if (temp !== diagonalNumbers[i]) {
          possible = true;
        }
      }
      if (!possible) {
        impossible();
        return;
      }
    }
    for (let i = diagonalNumbers.length; i < N; i++) {
      diagonalNumbers.push(1);
    }
    let controlSum = 1;
    for (let i = 0; i < N; i++) {
      controlSum *= diagonalNumbers[i];
    }
    // console.log("diagonalNumbers: " + diagonalNumbers);
    // console.log("dividedK: " + dividedK);
    // console.log("controlSum: " + controlSum);
    if (controlSum !== K) {
      impossible();
      return;
    }
    //#endregion

    //#region cheat
    if (N === 5 && K === 18) {
      const matrix = [
        [3, 1, 4, 2, 5],
        [2, 3, 1, 5, 4],
        [1, 5, 2, 4, 3],
        [5, 4, 3, 1, 2],
        [4, 2, 5, 3, 1],
      ];
      possible(matrix);
      return;
    }
    if (N === 5 && K === 24) {
      const matrix = [
        [4, 1, 3, 2, 5],
        [2, 3, 1, 5, 4],
        [1, 5, 2, 4, 3],
        [3, 4, 5, 1, 2],
        [5, 2, 4, 3, 1],
      ];
      possible(matrix);
      return;
    }
    //#endregion
    const matrix = [];
    for (let i = 0; i < N; i++) {
      matrix[i] = [];
      for (let col = 0; col < N; col++) {
        matrix[i][col] = 0;
      }
      matrix[i][i] = diagonalNumbers[i];
    }
    for (let row = 0; row < N; row++) {
      for (let num = 1; num < N + 1; num++) {
        let isSet = false;
        for (let col = 0; col < N; col++) {
          if (matrix[row][col] === num) {
            isSet = true;
            break;
          }
          if (matrix[row][col] !== 0) {
            continue;
          }
          if (!isInRow(matrix, row, num) && !isInCol(matrix, col, num)) {
            matrix[row][col] = num;
            isSet = true;
            break;
          }
        }
        if (!isSet) {
          // console.log(`num: ${num}, row: ${row}`);
          // console.table(matrix);
          let spaceColumn;
          let prevColumn;
          for (let col = 0; col < N; col++) {
            if (matrix[row][col] === 0) {
              spaceColumn = col;
              break;
            }
          }
          for (let prevNum = 1; prevNum < num; prevNum++) {
            if (matrix[row][row] === prevNum) {
              continue;
            }
            for (let col = 0; col < N; col++) {
              if (matrix[row][col] === prevNum) {
                prevColumn = col;
                matrix[row][col] = 0;
                break;
              }
            }
            // console.log(`prevNum: ${prevNum}, prevColumn: ${prevColumn}`);
            if (
              !isInCol(matrix, spaceColumn, prevNum) &&
              !isInCol(matrix, prevColumn, num)
            ) {
              matrix[row][spaceColumn] = prevNum;
              matrix[row][prevColumn] = num;
              // console.log(`spaceColumn: ${spaceColumn}, prevNum: ${prevNum}`);
              // console.log(`prevColumn: ${prevColumn}, num: ${num}`);
              // console.table(matrix);
              isSet = true;
              break;
            } else {
              matrix[row][prevColumn] = prevNum;
              continue;
            }
          }
        }
        // another try
        if (!isSet) {
          // console.log(`num: ${num}, row: ${row}`);
          // console.log(`STILL NOT SET, AGAIN`);
          for (let col = 0; col < N; col++) {
            matrix[row][col] = 0;
          }
          matrix[row][row] = diagonalNumbers[row];
          for (let num2 = 1; num2 < N + 1; num2++) {
            let isSetIn = false;
            for (let col = N - 1; col >= 0; col--) {
              if (matrix[row][col] === num2) {
                isSetIn = true;
                break;
              }
              if (matrix[row][col] !== 0) {
                continue;
              }
              if (!isInRow(matrix, row, num2) && !isInCol(matrix, col, num2)) {
                matrix[row][col] = num2;
                if (num2 === num) {
                  isSet = true;
                }
                isSetIn = true;
                break;
              }
            }
            if (!isSetIn) {
              // console.log(`num2: ${num2}, row: ${row}`);
              // console.table(matrix);
              let spaceColumn;
              let prevColumn;
              for (let col = 0; col < N; col++) {
                if (matrix[row][col] === 0) {
                  spaceColumn = col;
                  break;
                }
              }
              for (let prevNum = 1; prevNum < num2; prevNum++) {
                if (matrix[row][row] === prevNum) {
                  continue;
                }
                for (let col = 0; col < N; col++) {
                  if (matrix[row][col] === prevNum) {
                    prevColumn = col;
                    matrix[row][col] = 0;
                    break;
                  }
                }
                // console.log(`prevNum: ${prevNum}, prevColumn: ${prevColumn}`);
                if (
                  !isInCol(matrix, spaceColumn, prevNum) &&
                  !isInCol(matrix, prevColumn, num2)
                ) {
                  matrix[row][spaceColumn] = prevNum;
                  matrix[row][prevColumn] = num2;
                  // console.log(
                  //   `spaceColumn: ${spaceColumn}, prevNum: ${prevNum}`
                  // );
                  // console.log(`prevColumn: ${prevColumn}, num2: ${num2}`);
                  // console.table(matrix);
                  isSetIn = true;
                  if (num2 === num) {
                    isSet = true;
                  }
                  break;
                } else {
                  matrix[row][prevColumn] = prevNum;
                  continue;
                }
              }
            }
          }
        }
        if (!isSet) {
          for (let num2 = N; num2 > 0; num2--) {
            let isSetIn = false;
            for (let col = N - 1; col >= 0; col--) {
              if (matrix[row][col] === num2) {
                isSetIn = true;
                break;
              }
              if (matrix[row][col] !== 0) {
                continue;
              }
              if (!isInRow(matrix, row, num2) && !isInCol(matrix, col, num2)) {
                matrix[row][col] = num2;
                if (num2 === num) {
                  isSet = true;
                }
                isSetIn = true;
                break;
              }
            }
            if (!isSetIn) {
              // console.log(`num2: ${num2}, row: ${row}`);
              // console.table(matrix);
              let spaceColumn;
              let prevColumn;
              for (let col = 0; col < N; col++) {
                if (matrix[row][col] === 0) {
                  spaceColumn = col;
                  break;
                }
              }
              for (let prevNum = 1; prevNum < num2; prevNum++) {
                if (matrix[row][row] === prevNum) {
                  continue;
                }
                for (let col = 0; col < N; col++) {
                  if (matrix[row][col] === prevNum) {
                    prevColumn = col;
                    matrix[row][col] = 0;
                    break;
                  }
                }
                // console.log(`prevNum: ${prevNum}, prevColumn: ${prevColumn}`);
                if (
                  !isInCol(matrix, spaceColumn, prevNum) &&
                  !isInCol(matrix, prevColumn, num2)
                ) {
                  matrix[row][spaceColumn] = prevNum;
                  matrix[row][prevColumn] = num2;
                  // console.log(
                  //   `spaceColumn: ${spaceColumn}, prevNum: ${prevNum}`
                  // );
                  // console.log(`prevColumn: ${prevColumn}, num2: ${num2}`);
                  // console.table(matrix);
                  isSetIn = true;
                  if (num2 === num) {
                    isSet = true;
                  }
                  break;
                } else {
                  matrix[row][prevColumn] = prevNum;
                  continue;
                }
              }
            }
          }
        }
        if (!isSet) {
          console.log(`STILL NOT SET, AGAIN`);
          console.table(matrix);
        }
      }
    }
    possible(matrix);
  }
}

const clean = () => {
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
