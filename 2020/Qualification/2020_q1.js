/**
 * Qualification Round 2020 - Google Code Jam 2020
 * Vestigium (7pts)
 * result: DONE
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let testCases;
let lines = 0;
const trace = [];

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/2020_q1_in"),
  input: fs.createReadStream("./2020_q1_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

function processLine(line) {
  if (!testCases) {
    testCases = +line;
    return;
  } else if (lines === 0) {
    lines = +line;
    return;
  } else {
    trace.push(line.split(" "));
    lines--;
    if (lines === 0) {
      let columns = 0;
      let rows = 0;
      let sum = 0;
      for (let i = 0; i < trace.length; i++) {
        // console.log(`columns #${i}:`);
        for (let j = 0; j < trace.length; j++) {
          for (let k = j + 1; k < trace.length; k++) {
            if (trace[j][i] === trace[k][i]) {
              // console.log(`1 - ${trace[j][i]}`);
              // console.log(`2 - ${trace[k][i]}`);
              columns++;
              j = trace.length;
              break;
            }
          }
        }
      }
      for (let i = 0; i < trace.length; i++) {
        // console.log(`rows #${i}:`);
        for (let j = 0; j < trace.length; j++) {
          for (let k = j + 1; k < trace.length; k++) {
            if (trace[i][j] === trace[i][k]) {
              // console.log(`1 - ${trace[i][j]}`);
              // console.log(`2 - ${trace[i][k]}`);
              rows++;
              j = trace.length;
              break;
            }
          }
        }
      }
      for (let i = 0; i < trace.length; i++) {
        sum += +trace[i][i];
      }
      trace.length = 0;
      console.log(`Case #${caseIndex++}: ${sum} ${rows} ${columns}`);
      return;
    }
  }
}
const clean = () => {
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
