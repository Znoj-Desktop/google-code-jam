/**
 * A2 Round 2020 - Google Code Jam 2020
 * Join the Ranks (14pts, 23pts)
 * result: 0pts, visible tests passed
 */

 const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let lines;

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/1B/2020_b3_in"),
  input: fs.createReadStream("./2020_b3_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

function processLine(line) {
  if (!lines) {
    lines = +line;
    return;
  } else {
    const [rS, sS] = line.split(" ");
    const R = +rS;
    const S = +sS;
    let deck = [];
    for (let i = 1; i <= S; i++) {
      for (let j = 1; j <= R; j++) {
        deck.push(j);
      }
    }
    // console.table(deck);
    const result = [];
    for (let x = 0; x < R; x++) {
      let first = deck[0];
      let a;
      let tempIndex;
      for (let i = 1; i < deck.length; i++) {
        if (first !== deck[i]) {
          a = deck[i];
          tempIndex = i;
          break;
        }
      }
      let b;
      let aIndex;
      for (let i = tempIndex; i < deck.length; i++) {
        if (a !== deck[i]) {
          b = deck[i];
          aIndex = i;
          break;
        }
      }
      if (b) {
        let bIndex;
        for (let i = deck.length - 1; i >= 0; i--) {
          if (first === deck[i]) {
            bIndex = i;
            break;
          }
        }
        if (bIndex < aIndex) {
          break;
        }
        result.push([aIndex, bIndex - aIndex + 1]);

        // console.log(`a: ${a}, b: ${b}`);
        // console.log(`aIndex: ${aIndex}, bIndex: ${bIndex}`);
        deck = deck
          .slice(aIndex, bIndex + 1)
          .concat(deck.slice(0, aIndex))
          .concat(deck.slice(bIndex + 1));
        // console.table(deck);
      } else {
        break;
      }
    }
    console.log(`Case #${caseIndex++}: ${result.length}`);
    for (let i = 0; i < result.length; i++) {
      console.log(`${result[i][0]} ${result[i][1]}`);
    }
  }
}

const clean = () => {
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
