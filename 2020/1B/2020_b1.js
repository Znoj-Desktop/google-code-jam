/**
 * A1 Round 2020 - Google Code Jam 2020
 * Expogo (5pts, 8pts, 16pts)
 * result: 13pts
 */

const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let lines;

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/1B/2020_b1_in"),
  input: fs.createReadStream("./2020_b1_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

let destX;
let destY;

let found;

// TODO need Dynamic programming
const go = (curX, curY, jump) => {
  if (curX === destX && curY === destY) {
    found = true;
    return true;
  }
  if (found || jump > 10000) {
    return false;
  }
  let path = "";

  const N = go(curX, curY + jump, jump * 2); // N
  if (N === true) {
    return "N";
  }
  const S = go(curX, curY - jump, jump * 2); // S
  if (S === true) {
    return "S";
  }
  const E = go(curX + jump, curY, jump * 2); // E
  if (E === true) {
    return "E";
  }
  const W = go(curX - jump, curY, jump * 2); // W
  if (W === true) {
    return "W";
  }
  if (N && (path.length === 0 || N.length < path.length)) {
    path = "N" + N;
  }
  if (S && (path.length === 0 || S.length < path.length)) {
    path = "S" + S;
  }
  if (E && (path.length === 0 || E.length < path.length)) {
    path = "E" + E;
  }
  if (W && (path.length === 0 || W.length < path.length)) {
    path = "W" + W;
  }
  return path;
};

function processLine(line) {
  if (!lines) {
    lines = +line;
    return;
  } else {
    const [xS, yS] = line.split(" ");
    destX = +xS;
    destY = +yS;
    found = false;
    const result = go(0, 0, 1) || "IMPOSSIBLE";
    console.log(`Case #${caseIndex++}: ${result}`);
  }
}

const clean = () => {
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
