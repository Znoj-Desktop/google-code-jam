const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let lines;

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/1C/2020_c1_in"),
  input: fs.createReadStream("./2020_c1_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});
let pepCoordinates = [];

let beginX;
let beginY;

let found;
let visited = [];

const setPepCoordinates = (x, y, path) => {
  pepCoordinates.length = 0;
  pepCoordinates.push([x, y]);
  for (let i = 0; i < path.length; i++) {
    if (path[i] === "N") {
      y += 1;
    } else if (path[i] === "E") {
      x += 1;
    } else if (path[i] === "S") {
      y -= 1;
    } else if (path[i] === "W") {
      x -= 1;
    }
    pepCoordinates.push([x, y]);
  }
};

// TODO need Dynamic programming
const go = (curX, curY, jump) => {
  if (
    visited[curX + 1000][curY + 1000] &&
    visited[curX + 1000][curY + 1000] > jump
  ) {
    visited[curX + 1000][curY + 1000] = jump;
  }
  if (found && jump >= found) {
    return false;
  }
  // if (curX < 0 || curY < 0 || curX > 1000 || curY > 1000) {
  //   return false;
  // }

  if (jump > pepCoordinates.length - 1) {
    return false;
  }
  if (curX === pepCoordinates[jump][0] && curY === pepCoordinates[jump][1]) {
    found = jump;
    return true;
  }
  let path = "";

  let N, S, E, W;

  if (
    !visited[curX + 1000][curY + 1001] ||
    visited[curX + 1000][curY + 1001] >= jump
  ) {
    N = go(curX, curY + 1, jump + 1); // N
    if (N === true) {
      return "N";
    }
  }
  if (
    !visited[curX + 1000][curY + 1000 - 1] ||
    visited[curX + 1000][curY + 1000 - 1] >= jump
  ) {
    S = go(curX, curY - 1, jump + 1); // S
    if (S === true) {
      return "S";
    }
  }
  if (
    !visited[curX + 1001][curY + 1000] ||
    visited[curX + 1001][curY + 1000] >= jump
  ) {
    E = go(curX + 1, curY, jump + 1); // E
    if (E === true) {
      return "E";
    }
  }
  if (
    !visited[curX + 1000 - 1][curY + 1000] ||
    visited[curX + 1000 - 1][curY + 1000] >= jump
  ) {
    W = go(curX - 1, curY, jump + 1); // W
    if (W === true) {
      return "W";
    }
  }
  const wait = go(curX, curY, jump + 1); // W
  if (wait === true) {
    return "_";
  }
  if (N && (path.length === 0 || N.length < path.length)) {
    path = "N" + N;
  }
  if (S && (path.length === 0 || S.length < path.length)) {
    path = "S" + S;
  }
  if (E && (path.length === 0 || E.length < path.length)) {
    path = "E" + E;
  }
  if (W && (path.length === 0 || W.length < path.length)) {
    path = "W" + W;
  }
  if (wait && (path.length === 0 || wait.length < path.length)) {
    path = "_" + wait;
  }
  return path;
};

function processLine(line) {
  if (!lines) {
    lines = +line;
    return;
  } else {
    const [xS, yS, pepPath] = line.split(" ");
    beginX = +xS;
    beginY = +yS;
    for (let i = 0; i < 2000; i++) {
      visited[i] = new Array(2000).fill(false);
    }
    setPepCoordinates(beginX, beginY, pepPath);

    found = false;
    let result;
    result = go(0, 0, 0);
    result = result ? found : "IMPOSSIBLE";

    console.log(`Case #${caseIndex++}: ${result}`);
    visited.length = 0;
  }
}

const clean = () => {
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
