const fs = require("fs");
const readline = require("readline");
let caseIndex = 1;
let lines;

const rl = readline.createInterface({
  // input: process.stdin,
  // input: fs.createReadStream("./2020/1C/2020_c3_in"),
  input: fs.createReadStream("./2020_c3_in"),
  output: process.stdout,
});

rl.on("line", processLine).on("close", function () {
  process.exit(0);
});

let N;
let D;
function processLine(line) {
  if (!lines) {
    lines = +line;
    return;
  } else if (!N && !D) {
    const [Ns, Ds] = line.split(" ");
    N = +Ns;
    D = +Ds;
    return;
  } else {
    const slices = line.split(" ");
    let cuts = -1;
    if (D === 2) {
      for (let i = 0; i < slices.length - 1; i++) {
        if (cuts < 0) {
          for (let j = i + 1; j < slices.length; j++) {
            if (slices[i] === slices[j]) {
              cuts = 0;
              break;
            }
          }
        } else {
          break;
        }
      }
      if (cuts < 0) {
        cuts = 1;
      }
    } else if (D === 3) {
      for (let i = 0; i < slices.length - 2; i++) {
        if (cuts < 0) {
          for (let j = i + 1; j < slices.length - 1; j++) {
            if (cuts < 0) {
              for (let k = j + 1; k < slices.length; k++) {
                if (slices[i] === slices[j] && slices[k] === slices[j]) {
                  cuts = 0;
                  break;
                }
              }
            } else {
              break;
            }
          }
        } else {
          break;
        }
      }
      if (cuts < 0 && slices.length > 2) {
        for (let i = 0; i < slices.length - 1; i++) {
          if (cuts < 0) {
            for (let j = i + 1; j < slices.length; j++) {
              if (slices[i] === slices[j]) {
                cuts = 1;
                break;
              }
            }
          } else {
            break;
          }
        }
      }
      if (cuts < 0) {
        for (let i = 0; i < slices.length - 1; i++) {
          if (cuts < 0) {
            for (let j = 0; j < slices.length; j++) {
              if (i === j) {
                continue;
              }
              if (+slices[i] / 2 === +slices[j]) {
                cuts = 1;
                break;
              }
            }
          } else {
            break;
          }
        }
      }
      if (cuts < 0) {
        cuts = 2;
      }
    }

    console.log(`Case #${caseIndex++}: ${cuts}`);
    clean();
  }
}

const clean = () => {
  N = 0;
  D = 0;
  // testCases--;
};

const exit = () => {
  // console.log(`exit`); // TODO remove
  rl.close();
};
